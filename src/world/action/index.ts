import * as constants from '../constants'

type SetAction = SetMin | SetMax

interface SetMin {
  type: constants.SET_MIN
  value: number
}

interface SetMax {
  type: constants.SET_MAX
  value: number
}

const setMin = (value: number): SetMin => ({
  type: constants.SET_MIN,
  value
})

const setMax = (value: number): SetMax => ({
  type: constants.SET_MAX,
  value
})

export {
  SetAction,
  SetMin,
  SetMax,
  setMin,
  setMax
}
