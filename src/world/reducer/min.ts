import { SetAction } from '../action'
import * as constants from '../constants'

const min = (state  = 0, action: SetAction) => {
  switch (action.type) {
    case constants.SET_MIN:
      return action.value

    default:
      return state
  }
}

export default min
