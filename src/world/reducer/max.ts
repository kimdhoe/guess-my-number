import { SetAction } from '../action'
import * as constants from '../constants'

const max = (state  = 100, action: SetAction) => {
  switch (action.type) {
    case constants.SET_MAX:
      return action.value

    default:
      return state
  }
}

export default max
