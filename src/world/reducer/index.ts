import { combineReducers, Reducer } from 'redux'

import min from './min'
import max from './max'
import { StoreState } from '../type'

const reducer: Reducer<StoreState> = combineReducers({
  min,
  max
})

export default reducer
