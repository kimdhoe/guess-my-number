import { createStore } from 'redux'

import reducer from './reducer'
import { StoreState } from './type'

const store = createStore<StoreState>(reducer)

export default store
