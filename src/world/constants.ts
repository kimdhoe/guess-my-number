export const SET_MIN = 'SET_MIN'
export type SET_MIN = typeof SET_MIN

export const SET_MAX = 'SET_MAX'
export type SET_MAX = typeof SET_MAX
