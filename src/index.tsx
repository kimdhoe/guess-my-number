import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import store from './world/store'
import { AppContainer } from './App/'
import registerServiceWorker from './registerServiceWorker'
import './index.css'

ReactDOM.render(
  <Provider store={store}>
    <AppContainer />
  </Provider>,
  document.getElementById('root') as HTMLElement
)

registerServiceWorker()
