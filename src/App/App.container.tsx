import { connect, Dispatch } from 'react-redux'

import { StoreState } from '../world/type'
import * as action from '../world/action'
import AppComponent from './App.component'

const makeGuess = (min: number, max: number): number =>
  Math.ceil((min + max) / 2)

const mapStateToProps = ({ min, max }: StoreState) => ({
  guess: makeGuess(min, max)
})

const mapDispatchToProps = (dispatch: Dispatch<action.SetAction>) => ({
  setMin: (value: number) => dispatch(action.setMin(value)),
  setMax: (value: number) => dispatch(action.setMax(value)),
})

const AppContainer = connect(mapStateToProps, mapDispatchToProps)(AppComponent)

export default AppContainer
