import * as React from 'react'

import './App.css'

interface Props {
  guess: number
  setMin: (value: number) => void
  setMax: (value: number) => void
}

const App = ({ guess, setMin, setMax }: Props)  => (
  <div className="App">
    <div className="App-header">
      <h1>Professor X</h1>
      <h2>Pick a number between 1 and 100.</h2>
    </div>
    <p className="App-intro">
      Is it {guess}?
    </p>
    <div>
      <button onClick={() => setMin(guess)}>
        It's greater than {guess}.
      </button>
      <button onClick={() => setMax(guess)}>
        It's less than {guess}.
      </button>
    </div>
  </div>
)

export default App
export { Props }
