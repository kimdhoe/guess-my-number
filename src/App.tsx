import * as React from 'react'
import './App.css'

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Guess My Number</h2>
        </div>
        <p className="App-intro">
          Is it 55?
        </p>

        <button>
          +
        </button>
        <button>
          -
        </button>
      </div>
    )
  }
}

export default App
